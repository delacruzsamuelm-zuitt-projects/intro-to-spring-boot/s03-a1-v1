package com.delacruz.b142.s02.s02app.models;

import javax.persistence.*;

@Entity
@Table(name="posts")
public class Post {

    // Properties (columns)
    @Id
    @GeneratedValue
    private Long id; // primary key
    @Column
    private String title;
    @Column
    private String content;

    // Constructors ()
    // Data: title, content

    // Empty
    public Post() {}

    // Parameterized
    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    // Getters & Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
