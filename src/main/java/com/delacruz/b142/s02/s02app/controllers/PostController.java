package com.delacruz.b142.s02.s02app.controllers;

import com.delacruz.b142.s02.s02app.models.Post;
import com.delacruz.b142.s02.s02app.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    // Creating a new post
    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody Post newPost) {
        postService.createPost(newPost);
        return new ResponseEntity<>("New post was created.", HttpStatus.CREATED);
    }

    // Retrieve all posts
    @RequestMapping(value="/posts", method=RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    // Update an existing post
    @RequestMapping(value = "/posts/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long id, @RequestBody Post updatedPost) {
        postService.updatePost(id, updatedPost);
        return new ResponseEntity<>("Post was successfully updated!", HttpStatus.OK);
    }

    // Delete an existing post
    @RequestMapping(value = "/posts/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long id) {
        postService.deletePost(id);
        return new ResponseEntity<>("Post was successfully deleted!", HttpStatus.OK);
    }

}

